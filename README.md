# The Challenge

The goal of this challenge is to build a web service that exposes for a given time frame:

- Vehicle (Bus)
- Fleet (Operator)
- Activity (Stop) data

The service exposes a **RESTful API** to answer the following questions:

1. Given a time frame `[start-time, end-time]`, what is the list of running operators?
1. Given a time frame `[start-time, end-time]` and an Operator, what is the list of vehicle IDs?
1. Given a time frame `[start-time, end-time]` and a fleet, which vehicles are at a stop?
1. Given a time frame `[start-time, end-time]` and a vehicle, return the trace of that vehicle (GPS entries, ordered by timestamp).

`Disclaimer: By running operators we mean all the operators with events on the period.`

`Disclaimer: The fleet is the operator.`

Before implementing this exercise, you need to:

-  Choose a database management system that is more appropriate for building this web service (we value NoSQL databases, but a relational database may be also fine);
-  Choose a programming language that is type-safe and you are familiar with;
-  Choose a web toolkit that is able to handle high throughput in terms of number of requests (we are less worried about latency, but that's not to be neglected as well).

## What to deliver

- A git code repo, public or private, that can be accessed by our team, with the following:
- Instructions on how to install and/or access to the database;
- A data loader script;
- The code of the web service;
- Instructions on how to launch the HTTP service;
- Documentation and/or examples on how to use the API.

## Dataset to use

- Name: Dublin Bus GPS sample data from Dublin City Council
- URL: https://data.gov.ie/dataset/dublin-bus-gps-sample-data-from-dublin-city-council-insight-project
- Download one extract, and from that extract, use 1 example CSV as input

# Setup

## Requirements

- Java 11
- Maven 3

## Build and tests

On the project root run:

```bash
mvn clean install
```

## To start the HTTP server

On the project root run:

```bash
mvn spring-boot:run
```

## Access to the database

The service runs a `H2 in-memory database` which starts along the server.
To access the console, go to: http://localhost:8080/h2.

## Load the sample data

- Copy the content of the `load.sql` script located in the project root
- Go to the console as described above and connect:
    - **Driver Class:** org.h2.Driver
    - **JDBC URL:** jdbc:h2:mem:testdb
    - **User Name:** sa
    - **Password:**
- Paste the copied query from the `load.sql` script into the **SQL Statement** box.
- Change the `<PATH TO CSV>` in the script to the path of the sample csv downloaded from https://data.gov.ie/dataset/dublin-bus-gps-sample-data-from-dublin-city-council-insight-project
- Run and wait for the completion

## API endpoints

### Retrieve all running operators in a given time frame

Path: `/api/v1/operators`

| Parameter | Description | Options |
| --- | --- | --- |
| startTime | The initial instant of the time frame (inclusive). Needs to be formatted in ISO, i.e.: 2019-01-01T00:00:00.000 | |
| endTime | The final instant of the time frame (exclusive). Needs to be formatted in ISO, i.e.: 2019-01-01T00:00:00.000 | |

#### Example

For the sample csv: *siri.20121112.csv*.

```bash
> curl "http://localhost:8080/api/v1/operators?startTime=2012-11-12T00:00:02.000&endTime=2012-11-12T00:00:03.000"

[{"id":"CD"},{"id":"CF"},{"id":"D1"},{"id":"HN"},{"id":"PO"},{"id":"RD"},{"id":"SL"}]
```

### Retrieve all vehicles from an operator in a given time frame

Path: `/api/v1/operator/{operatorId}/vehicles`

| Path Variable | Description |
| --- | --- |
| operatorId | The id of the operator |

| Parameter | Description | Options |
| --- | --- | --- |
| startTime | The initial instant of the time frame (inclusive). Needs to be formatted in ISO, i.e.: 2019-01-01T00:00:00.000 | |
| endTime | The final instant of the time frame (exclusive). Needs to be formatted in ISO, i.e.: 2019-01-01T00:00:00.000 | |
| atStop | Indicate if should retrieve vehicles at a stop. | AT_STOP, NOT_AT_STOP, BOTH (default) |

#### Example

For the sample csv: *siri.20121112.csv*.

```bash
> curl "http://localhost:8080/api/v1/operator/CD/vehicles?startTime=2012-11-12T00:00:02.000&endTime=2012-11-12T00:00:03.000"

[{"id":"33226"},{"id":"33291"},{"id":"33353"},{"id":"33521"},{"id":"38062"}]

> curl "http://localhost:8080/api/v1/operator/CD/vehicles?startTime=2012-11-12T00:00:02.000&endTime=2012-11-12T00:00:03.000&atStop=AT_STOP"

[{"id":"33226"}]

> curl "http://localhost:8080/api/v1/operator/CD/vehicles?startTime=2012-11-12T00:00:02.000&endTime=2012-11-12T00:00:03.000&atStop=NOT_AT_STOP"

[{"id":"33291"},{"id":"33353"},{"id":"33521"},{"id":"38062"}]

> curl "http://localhost:8080/api/v1/operator/CD/vehicles?startTime=2012-11-12T00:00:02.000&endTime=2012-11-12T00:00:03.000&atStop=BOTH"

[{"id":"33226"},{"id":"33291"},{"id":"33353"},{"id":"33521"},{"id":"38062"}]
```

### Retrieve all traffic events from a vehicle in a given time frame

Path: `/api/v1/vehicle/{vehicleId}/events`

| Path Variable | Description |
| --- | --- |
| vehicleId | The id of the vehicle |

| Parameter | Description | Options |
| --- | --- | --- |
| startTime | The initial instant of the time frame (inclusive). Needs to be formatted in ISO, i.e.: 2019-01-01T00:00:00.000 | |
| endTime | The final instant of the time frame (exclusive). Needs to be formatted in ISO, i.e.: 2019-01-01T00:00:00.000 | |

#### Example

For the sample csv: *siri.20121112.csv*.

```bash
> curl "http://localhost:8080/api/v1/vehicle/33226/events?startTime=2012-11-12T00:00:02.000&endTime=2012-11-12T00:00:03.000"

[{"timestamp":"2012-11-12T00:00:02","operator":{"id":"CD"},"vehicle":{"id":"33226"},"atStop":{"atStop":true},"latitude":53.3389,"longitude":-6.373318}]
```