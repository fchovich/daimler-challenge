package daimler.challenge.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.time.LocalDateTime;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@Value.Immutable
@JsonSerialize(as = ImmutableTrafficEvent.class)
@JsonDeserialize(as = ImmutableTrafficEvent.class)
public interface TrafficEvent {
  LocalDateTime timestamp();

  OperatorId operator();

  VehicleId vehicle();

  VehicleAtStop atStop();

  Float latitude();

  Float longitude();
}
