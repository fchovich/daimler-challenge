package daimler.challenge.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@Value.Immutable
@JsonSerialize(as = ImmutableVehicleAtStop.class)
@JsonDeserialize(as = ImmutableVehicleAtStop.class)
public interface VehicleAtStop {
  boolean atStop();
}
