package daimler.challenge.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@Value.Immutable
@JsonSerialize(as = ImmutableVehicleId.class)
@JsonDeserialize(as = ImmutableVehicleId.class)
public interface VehicleId {
  String id();
}
