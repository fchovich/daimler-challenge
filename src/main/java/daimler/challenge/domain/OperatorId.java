package daimler.challenge.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@Value.Immutable
@JsonSerialize(as = ImmutableOperatorId.class)
@JsonDeserialize(as = ImmutableOperatorId.class)
public interface OperatorId {
  String id();
}
