package daimler.challenge.service;

import daimler.challenge.domain.OperatorId;
import daimler.challenge.domain.TrafficEvent;
import daimler.challenge.domain.VehicleId;
import daimler.challenge.repository.TimeFrameFilter;
import daimler.challenge.repository.VehicleAtStopFilter;

import java.util.List;

/**
 * @author fernando.chovich
 * @since 1.0
 */
public interface TrafficMonitorService {

  List<OperatorId> getAllRunningOperators(final TimeFrameFilter timeFrameFilter);

  List<VehicleId> getVehicles(
      final TimeFrameFilter timeFrameFilter,
      final OperatorId operator,
      final VehicleAtStopFilter atStop);

  List<TrafficEvent> getTrafficEvents(
      final TimeFrameFilter timeFrameFilter, final VehicleId vehicle);
}
