package daimler.challenge.service;

import daimler.challenge.domain.OperatorId;
import daimler.challenge.domain.TrafficEvent;
import daimler.challenge.domain.VehicleAtStop;
import daimler.challenge.domain.VehicleId;
import daimler.challenge.repository.TimeFrameFilter;
import daimler.challenge.repository.VehicleAtStopFilter;
import daimler.challenge.repository.VehicleTrafficDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@Service
public class TrafficMonitorServiceImpl implements TrafficMonitorService {

  @Autowired private VehicleTrafficDataRepository repository;

  @Override
  public List<OperatorId> getAllRunningOperators(final TimeFrameFilter timeFrameFilter) {
    return this.repository.findAllOperatorsWithEvents(timeFrameFilter);
  }

  @Override
  public List<VehicleId> getVehicles(
      final TimeFrameFilter timeFrameFilter,
      final OperatorId operator,
      final VehicleAtStopFilter atStop) {
    return this.repository.findAllVehiclesByOperatorWithEvents(timeFrameFilter, operator, atStop);
  }

  @Override
  public List<TrafficEvent> getTrafficEvents(
      final TimeFrameFilter timeFrameFilter, final VehicleId vehicle) {
    return this.repository.findAllTrafficEventsByVehicle(timeFrameFilter, vehicle);
  }
}
