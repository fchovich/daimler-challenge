package daimler.challenge.repository;

/**
 * @author fernando.chovich
 * @since 1.0
 */
public enum VehicleAtStopFilter {
  AT_STOP,
  NOT_AT_STOP,
  BOTH
}
