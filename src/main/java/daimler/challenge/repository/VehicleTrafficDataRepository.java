package daimler.challenge.repository;

import daimler.challenge.domain.OperatorId;
import daimler.challenge.domain.TrafficEvent;
import daimler.challenge.domain.VehicleAtStop;
import daimler.challenge.domain.VehicleId;

import java.util.List;

/**
 * @author fernando.chovich
 * @since 1.0
 */
public interface VehicleTrafficDataRepository {

  List<OperatorId> findAllOperatorsWithEvents(final TimeFrameFilter timeFrameFilter);

  List<VehicleId> findAllVehiclesByOperatorWithEvents(
      final TimeFrameFilter timeFrameFilter, final OperatorId operator, final VehicleAtStopFilter atStop);

  List<TrafficEvent> findAllTrafficEventsByVehicle(
      final TimeFrameFilter timeFrameFilter, final VehicleId vehicle);
}
