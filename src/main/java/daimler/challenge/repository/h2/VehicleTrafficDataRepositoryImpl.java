package daimler.challenge.repository.h2;

import daimler.challenge.domain.ImmutableOperatorId;
import daimler.challenge.domain.ImmutableTrafficEvent;
import daimler.challenge.domain.ImmutableVehicleAtStop;
import daimler.challenge.domain.ImmutableVehicleId;
import daimler.challenge.domain.OperatorId;
import daimler.challenge.domain.TrafficEvent;
import daimler.challenge.domain.VehicleId;
import daimler.challenge.repository.TimeFrameFilter;
import daimler.challenge.repository.VehicleAtStopFilter;
import daimler.challenge.repository.VehicleTrafficDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static daimler.challenge.repository.VehicleAtStopFilter.AT_STOP;
import static daimler.challenge.repository.VehicleAtStopFilter.BOTH;
import static java.util.stream.Collectors.toList;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@Repository
public class VehicleTrafficDataRepositoryImpl implements VehicleTrafficDataRepository {

  private final EntityManager entityManager;

  @Autowired
  public VehicleTrafficDataRepositoryImpl(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  @Override
  public List<OperatorId> findAllOperatorsWithEvents(final TimeFrameFilter timeFrameFilter) {

    final CriteriaBuilder criteria = this.entityManager.getCriteriaBuilder();
    final CriteriaQuery<String> query = criteria.createQuery(String.class);
    final Root<VehicleTrafficData> trafficData = query.from(VehicleTrafficData.class);

    query.select(trafficData.get("operatorId")).distinct(true);
    query.where(
        criteria.greaterThanOrEqualTo(trafficData.get("timestamp"), timeFrameFilter.startTime()),
        criteria.lessThan(trafficData.get("timestamp"), timeFrameFilter.endTime()));

    return this.entityManager
        .createQuery(query)
        .getResultStream()
        .map(id -> ImmutableOperatorId.builder().id(id).build())
        .map(OperatorId.class::cast)
        .collect(toList());
  }

  @Override
  public List<VehicleId> findAllVehiclesByOperatorWithEvents(
      final TimeFrameFilter timeFrameFilter,
      final OperatorId operator,
      final VehicleAtStopFilter vehicleAtStop) {
    final CriteriaBuilder criteria = this.entityManager.getCriteriaBuilder();
    final CriteriaQuery<String> query = criteria.createQuery(String.class);
    final Root<VehicleTrafficData> trafficData = query.from(VehicleTrafficData.class);

    final List<Predicate> predicates = new ArrayList<Predicate>();
    predicates.add(
        criteria.greaterThanOrEqualTo(trafficData.get("timestamp"), timeFrameFilter.startTime()));
    predicates.add(criteria.lessThan(trafficData.get("timestamp"), timeFrameFilter.endTime()));
    predicates.add(criteria.equal(trafficData.get("operatorId"), operator.id()));

    if (!BOTH.equals(vehicleAtStop)) {
      predicates.add(criteria.equal(trafficData.get("atStop"), AT_STOP.equals(vehicleAtStop)));
    }

    query.select(trafficData.get("vehicleId")).distinct(true);
    query.where(predicates.toArray(new Predicate[] {}));

    return this.entityManager
        .createQuery(query)
        .getResultStream()
        .map(id -> ImmutableVehicleId.builder().id(id).build())
        .collect(toList());
  }

  @Override
  public List<TrafficEvent> findAllTrafficEventsByVehicle(
      final TimeFrameFilter timeFrameFilter, final VehicleId vehicle) {
    final CriteriaBuilder criteria = this.entityManager.getCriteriaBuilder();
    final CriteriaQuery<VehicleTrafficData> query = criteria.createQuery(VehicleTrafficData.class);
    final Root<VehicleTrafficData> trafficData = query.from(VehicleTrafficData.class);

    query.where(
        criteria.greaterThanOrEqualTo(trafficData.get("timestamp"), timeFrameFilter.startTime()),
        criteria.lessThan(trafficData.get("timestamp"), timeFrameFilter.endTime()),
        criteria.equal(trafficData.get("vehicleId"), vehicle.id()));

    return this.entityManager
        .createQuery(query)
        .getResultStream()
        .map(
            data ->
                ImmutableTrafficEvent.builder()
                    .timestamp(data.getTimestamp())
                    .operator(ImmutableOperatorId.builder().id(data.getOperatorId()).build())
                    .vehicle(ImmutableVehicleId.builder().id(data.getVehicleId()).build())
                    .atStop(ImmutableVehicleAtStop.builder().atStop(data.isAtStop()).build())
                    .latitude(data.getLatitude())
                    .longitude(data.getLongitude())
                    .build())
        .collect(toList());
  }
}
