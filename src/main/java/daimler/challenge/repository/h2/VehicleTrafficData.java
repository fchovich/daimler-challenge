package daimler.challenge.repository.h2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@Entity
@Table(name = "VEHICLE_TRAFFIC_DATA")
public class VehicleTrafficData {
  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "TIMESTAMP")
  private LocalDateTime timestamp;

  @Column(name = "OPERATOR_ID")
  private String operatorId;

  @Column(name = "VEHICLE_ID")
  private String vehicleId;

  @Column(name = "LATITUDE", precision = 6)
  private Float latitude;

  @Column(name = "LONGITUDE", precision = 6)
  private Float longitude;

  @Column(name = "AT_STOP")
  private boolean atStop;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(final LocalDateTime timestamp) {
    this.timestamp = timestamp;
  }

  public String getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(final String operatorId) {
    this.operatorId = operatorId;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(final String vehicleId) {
    this.vehicleId = vehicleId;
  }

  public Float getLatitude() {
    return latitude;
  }

  public void setLatitude(final Float latitude) {
    this.latitude = latitude;
  }

  public Float getLongitude() {
    return longitude;
  }

  public void setLongitude(final Float longitude) {
    this.longitude = longitude;
  }

  public boolean isAtStop() {
    return atStop;
  }

  public void setAtStop(final boolean atStop) {
    this.atStop = atStop;
  }
}
