package daimler.challenge.repository;

import org.immutables.value.Value;

import java.time.LocalDateTime;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@Value.Immutable
public interface TimeFrameFilter {

  LocalDateTime startTime();

  LocalDateTime endTime();
}
