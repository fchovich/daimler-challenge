package daimler.challenge.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("daimler.challenge")
@EntityScan("daimler.challenge")
public class TrafficMonitorWebApp {

  public static void main(String[] args) {
    SpringApplication.run(TrafficMonitorWebApp.class, args);
  }
}
