package daimler.challenge.web;

import daimler.challenge.domain.ImmutableOperatorId;
import daimler.challenge.domain.ImmutableVehicleId;
import daimler.challenge.domain.OperatorId;
import daimler.challenge.domain.TrafficEvent;
import daimler.challenge.domain.VehicleId;
import daimler.challenge.repository.ImmutableTimeFrameFilter;
import daimler.challenge.repository.TimeFrameFilter;
import daimler.challenge.repository.VehicleAtStopFilter;
import daimler.challenge.service.TrafficMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@RestController
@RequestMapping("/api/v1/")
public class TrafficMonitorRestController {

  @Autowired private TrafficMonitorService trafficMonitorService;

  @GetMapping("/operators")
  public List<OperatorId> getRunningOperators(
      @RequestParam @DateTimeFormat(iso = DATE_TIME) final LocalDateTime startTime,
      @RequestParam @DateTimeFormat(iso = DATE_TIME) final LocalDateTime endTime) {
    final TimeFrameFilter timeFrameFilter =
        ImmutableTimeFrameFilter.builder().startTime(startTime).endTime(endTime).build();
    return this.trafficMonitorService.getAllRunningOperators(timeFrameFilter);
  }

  @GetMapping("/operator/{operatorId}/vehicles")
  public List<VehicleId> getVehicles(
      @RequestParam @DateTimeFormat(iso = DATE_TIME) final LocalDateTime startTime,
      @RequestParam @DateTimeFormat(iso = DATE_TIME) final LocalDateTime endTime,
      @RequestParam(defaultValue = "BOTH") final VehicleAtStopFilter atStop,
      @PathVariable final String operatorId) {
    final TimeFrameFilter timeFrameFilter =
        ImmutableTimeFrameFilter.builder().startTime(startTime).endTime(endTime).build();

    return this.trafficMonitorService.getVehicles(
        timeFrameFilter, ImmutableOperatorId.builder().id(operatorId).build(), atStop);
  }

  @GetMapping("/vehicle/{vehicleId}/events")
  public List<TrafficEvent> getTrafficEvents(
      @RequestParam @DateTimeFormat(iso = DATE_TIME) final LocalDateTime startTime,
      @RequestParam @DateTimeFormat(iso = DATE_TIME) final LocalDateTime endTime,
      @PathVariable final String vehicleId) {
    final TimeFrameFilter timeFrameFilter =
        ImmutableTimeFrameFilter.builder().startTime(startTime).endTime(endTime).build();
    return this.trafficMonitorService.getTrafficEvents(
        timeFrameFilter, ImmutableVehicleId.builder().id(vehicleId).build());
  }
}
