package daimler.challenge.web;

import daimler.challenge.domain.ImmutableOperatorId;
import daimler.challenge.domain.OperatorId;
import daimler.challenge.repository.ImmutableTimeFrameFilter;
import daimler.challenge.repository.TimeFrameFilter;
import daimler.challenge.repository.h2.VehicleTrafficData;
import daimler.challenge.repository.h2.VehicleTrafficDataRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author fernando.chovich
 * @since 1.0
 */
@SuppressWarnings("UnusedLabel")
@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
public class VehicleTrafficDataRepositoryImplTest {

  @Autowired private TestEntityManager entityManager;

  @Autowired private VehicleTrafficDataRepositoryImpl repository;

  @Test
  public void querySuccessfully() {
    final LocalDateTime referenceDate;
    final VehicleTrafficData eventWithinBoundary,
        eventOnUpperBoundaryLimit,
        eventOnLowerBoundaryLimit,
        eventOutsideLowerBoundary,
        eventOutsideUpperBoundary;
    final TimeFrameFilter timeFrameFilter;
    final List<OperatorId> operators;

    given:
    referenceDate = LocalDateTime.now();

    // This should be returned by the query: timestamp greater than and less than start time and end
    // time, respectively
    eventWithinBoundary = new VehicleTrafficData();
    eventWithinBoundary.setTimestamp(referenceDate);
    eventWithinBoundary.setAtStop(false);
    eventWithinBoundary.setOperatorId("A");

    // Same as above, same operator to check unique projection
    eventOnUpperBoundaryLimit = new VehicleTrafficData();
    eventOnUpperBoundaryLimit.setTimestamp(referenceDate.plus(1, SECONDS));
    eventOnUpperBoundaryLimit.setAtStop(false);
    eventOnUpperBoundaryLimit.setOperatorId("B");

    // This should be returned by the query: timestamp equal to the start time
    eventOnLowerBoundaryLimit = new VehicleTrafficData();
    eventOnLowerBoundaryLimit.setTimestamp(referenceDate.minus(1, SECONDS));
    eventOnLowerBoundaryLimit.setAtStop(false);
    eventOnLowerBoundaryLimit.setOperatorId("C");

    // Should not be returned by the query: timestamp less than the start time
    eventOutsideLowerBoundary = new VehicleTrafficData();
    eventOutsideLowerBoundary.setTimestamp(referenceDate.minus(2, SECONDS));
    eventOutsideLowerBoundary.setAtStop(false);
    eventOutsideLowerBoundary.setOperatorId("D");

    // Should not be returned by the query: timestamp equal to the end time
    eventOutsideUpperBoundary = new VehicleTrafficData();
    eventOutsideUpperBoundary.setTimestamp(referenceDate.plus(3, SECONDS));
    eventOutsideUpperBoundary.setAtStop(false);
    eventOutsideUpperBoundary.setOperatorId("E");

    this.entityManager.persistAndFlush(eventWithinBoundary);
    this.entityManager.persistAndFlush(eventOnUpperBoundaryLimit);
    this.entityManager.persistAndFlush(eventOnLowerBoundaryLimit);
    this.entityManager.persistAndFlush(eventOutsideLowerBoundary);
    this.entityManager.persistAndFlush(eventOutsideUpperBoundary);

    timeFrameFilter =
        ImmutableTimeFrameFilter.builder()
            .startTime(referenceDate.minus(1, SECONDS))
            .endTime(referenceDate.plus(2, SECONDS))
            .build();

    when:
    operators = this.repository.findAllOperatorsWithEvents(timeFrameFilter);

    then:
    assertThat(operators)
        .hasSize(3)
        .containsExactlyInAnyOrder(
            ImmutableOperatorId.builder().id("A").build(),
            ImmutableOperatorId.builder().id("B").build(),
            ImmutableOperatorId.builder().id("C").build());
  }
}
